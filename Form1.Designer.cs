﻿namespace m3uPlay
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.OK = new System.Windows.Forms.Button();
            this.mainListBox = new System.Windows.Forms.CheckedListBox();
            this.RezervListBox = new System.Windows.Forms.CheckedListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.fillFileMain = new System.Windows.Forms.Button();
            this.descriptionText = new System.Windows.Forms.TextBox();
            this.AddItemList = new System.Windows.Forms.Button();
            this.AdresText = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.fillFileRezerv = new System.Windows.Forms.Button();
            this.openFileDialogMain = new System.Windows.Forms.OpenFileDialog();
            this.openFileDialogRezerv = new System.Windows.Forms.OpenFileDialog();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // OK
            // 
            this.OK.Location = new System.Drawing.Point(548, 431);
            this.OK.Name = "OK";
            this.OK.Size = new System.Drawing.Size(75, 23);
            this.OK.TabIndex = 0;
            this.OK.Text = "OK";
            this.OK.UseVisualStyleBackColor = true;
            // 
            // mainListBox
            // 
            this.mainListBox.FormattingEnabled = true;
            this.mainListBox.Location = new System.Drawing.Point(62, 64);
            this.mainListBox.Name = "mainListBox";
            this.mainListBox.Size = new System.Drawing.Size(268, 289);
            this.mainListBox.TabIndex = 1;
            // 
            // RezervListBox
            // 
            this.RezervListBox.FormattingEnabled = true;
            this.RezervListBox.Location = new System.Drawing.Point(363, 64);
            this.RezervListBox.Name = "RezervListBox";
            this.RezervListBox.Size = new System.Drawing.Size(260, 289);
            this.RezervListBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(59, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Основной Playlist";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(360, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Резервный Playlist";
            // 
            // fillFileMain
            // 
            this.fillFileMain.Location = new System.Drawing.Point(205, 359);
            this.fillFileMain.Name = "fillFileMain";
            this.fillFileMain.Size = new System.Drawing.Size(125, 23);
            this.fillFileMain.TabIndex = 5;
            this.fillFileMain.Text = " Заполнить из файла";
            this.fillFileMain.UseVisualStyleBackColor = true;
            this.fillFileMain.Click += new System.EventHandler(this.FillFileMain_Click);
            // 
            // descriptionText
            // 
            this.descriptionText.Location = new System.Drawing.Point(3, 3);
            this.descriptionText.Name = "descriptionText";
            this.descriptionText.Size = new System.Drawing.Size(178, 20);
            this.descriptionText.TabIndex = 6;
            // 
            // AddItemList
            // 
            this.AddItemList.Location = new System.Drawing.Point(198, 28);
            this.AddItemList.Name = "AddItemList";
            this.AddItemList.Size = new System.Drawing.Size(67, 23);
            this.AddItemList.TabIndex = 7;
            this.AddItemList.Text = "Добавить";
            this.AddItemList.UseVisualStyleBackColor = true;
            // 
            // AdresText
            // 
            this.AdresText.Location = new System.Drawing.Point(3, 30);
            this.AdresText.Name = "AdresText";
            this.AdresText.Size = new System.Drawing.Size(178, 20);
            this.AdresText.TabIndex = 8;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.descriptionText);
            this.panel1.Controls.Add(this.AdresText);
            this.panel1.Controls.Add(this.AddItemList);
            this.panel1.Location = new System.Drawing.Point(62, 398);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(268, 56);
            this.panel1.TabIndex = 9;
            // 
            // fillFileRezerv
            // 
            this.fillFileRezerv.Location = new System.Drawing.Point(498, 359);
            this.fillFileRezerv.Name = "fillFileRezerv";
            this.fillFileRezerv.Size = new System.Drawing.Size(125, 23);
            this.fillFileRezerv.TabIndex = 10;
            this.fillFileRezerv.Text = " Заполнить из файла";
            this.fillFileRezerv.UseVisualStyleBackColor = true;
            // 
            // openFileDialogMain
            // 
            this.openFileDialogMain.FileName = "openFileDialog1";
            this.openFileDialogMain.Filter = "*.m3u | *m3u8";
            this.openFileDialogMain.Title = "Основной лист";
            // 
            // openFileDialogRezerv
            // 
            this.openFileDialogRezerv.FileName = "openFileDialog1";
            this.openFileDialogRezerv.Filter = "*.m3u | *m3u8";
            this.openFileDialogRezerv.Multiselect = true;
            this.openFileDialogRezerv.Title = "Резервные листы";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 555);
            this.Controls.Add(this.fillFileRezerv);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.fillFileMain);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.RezervListBox);
            this.Controls.Add(this.mainListBox);
            this.Controls.Add(this.OK);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "m3uPlay";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button OK;
        private System.Windows.Forms.CheckedListBox mainListBox;
        private System.Windows.Forms.CheckedListBox RezervListBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button fillFileMain;
        private System.Windows.Forms.TextBox descriptionText;
        private System.Windows.Forms.Button AddItemList;
        private System.Windows.Forms.TextBox AdresText;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button fillFileRezerv;
        private System.Windows.Forms.OpenFileDialog openFileDialogMain;
        private System.Windows.Forms.OpenFileDialog openFileDialogRezerv;
    }
}

